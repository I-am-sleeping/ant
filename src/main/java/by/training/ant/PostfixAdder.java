package by.training.ant;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;

public class PostfixAdder extends Task {

    private static final String DELIMITER = "-";
    private String postfix = "";
    private List<FileSet> filesets = new ArrayList<>();

    public void addFileSet(FileSet fileset) {
        this.filesets.add(fileset);
    }

    public void setPostfix(String postfix) {
        this.postfix = postfix;
    }

    @Override
    public void execute() throws BuildException {
        for (FileSet fileset : filesets) {
            DirectoryScanner ds = fileset.getDirectoryScanner();
            String[] filenames = ds.getIncludedFiles();
            for (String filename : filenames) {
                File path = new File(ds.getBasedir().getAbsolutePath());
                File file = new File(path, filename);
                StringBuilder newFilename = new StringBuilder(filename);
                int i = newFilename.lastIndexOf(".");
                if (i == -1) {
                    newFilename.append(DELIMITER).append(postfix);
                } else {
                    newFilename.insert(i, DELIMITER + postfix);
                }
                File newFile = new File(path, newFilename.toString());
                if (newFile.exists()) {
                    log("File " + newFile.getName() + "already exists. File "
                            + filename + "can not be renamed.");
                } else {
                    boolean success = file.renameTo(newFile);
                    if (success) {
                        log("Renamed " + filename + " to " + newFile.getName());
                    } else {
                        log("Failed to rename " + filename + " to "
                                + newFile.getName());
                    }
                }
            }
        }
    }

}
